       IDENTIFICATION DIVISION. 
       PROGRAM-ID. MYGRADE.
       AUTHOR. WATCHARAPHON.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT MYGRADE-FILE ASSIGN TO "mygrade.txt"
              ORGANIZATION IS LINE SEQUENTIAL.
           SELECT AVG-GRADE-FILE ASSIGN TO "avg.txt"
              ORGANIZATION IS LINE SEQUENTIAL.
       
       DATA DIVISION. 
       FILE SECTION.
       FD  MYGRADE-FILE.
       01  MYGRADE-DETAIL.
           88 END-OF-MYGRADE-FILE     VALUE HIGH-VALUE. 
           05 SUBJECT.                 
              10 SUBJECT-ID           PIC X(6).
              10 SUBJECT-NAME         PIC X(50).
              10 SUBJECT-UNIT         PIC 9.
              10 GRADE                PIC X(2).
       FD  AVG-GRADE-FILE.
       01  AVG-GRADE-DETAIL.
           05 AVG-GRADE               PIC 9(3)V9(3).
           05 AVG-SCI-GRADE           PIC 9(3)V9(3).
           05 AVG-CS-GRADE            PIC 9(3)V9(3).

       WORKING-STORAGE SECTION. 
       01  AVG                        PIC 9(3)V9(3).
       01  AVG-SCI                    PIC 9(3)V9(3).
       01  AVG-COM-SCI                PIC 9(3)V9(3).

       01  SUMJECT-DETAIL.
           05 SUB-ID            PIC X(6).
           05 SUB-UNIT          PIC 9.
           05 SUB-GRADE         PIC X(2).
           05 TRANS-GRADE       PIC 9V9.

       01  ID-SCI REDEFINES SUMJECT-DETAIL.
           05 SINGLE-ID-SCI PIC A.
       01  ID-COM-SCI REDEFINES SUMJECT-DETAIL.
           05 SINGLE-ID-COMSCI PIC AA.

      *    AVG
       01  SUM-ALL-SCORE           PIC 9(3)v9(3).
       01  COUNT-ALL-UNIT          PIC 9(3)v9(3).
      *    AVG SCI
       01  SUM-SCORE-SCI           PIC 9(3)v9(3).
       01  COUNT-SCI-UNIT          PIC 9(3)v9(3).  
      *    AVG COM-SCI
       01  SUM-SCORE-COM-SCI       PIC 9(3)v9(3).
       01  COUNT-COM-SCI-UNIT      PIC 9(3)v9(3).
       01  COUNT-ALL               PIC 9(3)v9(3).
       
       PROCEDURE DIVISION .
       BEGIN.
           OPEN INPUT MYGRADE-FILE 
           OPEN OUTPUT AVG-GRADE-FILE 

           PERFORM UNTIL END-OF-MYGRADE-FILE
              READ MYGRADE-FILE
                 AT END SET END-OF-MYGRADE-FILE TO TRUE 
                 PERFORM 002-AVG
              END-READ
              IF NOT END-OF-MYGRADE-FILE THEN
                 PERFORM 001-PROCESS THRU 001-EXIT
              END-IF
           END-PERFORM

           CLOSE MYGRADE-FILE  
           CLOSE AVG-GRADE-FILE 
           GOBACK . 
       001-PROCESS.
           COMPUTE COUNT-ALL = SUB-UNIT + COUNT-ALL 
           MOVE SUBJECT-ID TO SUB-ID
           MOVE SUBJECT-UNIT TO SUB-UNIT

           EVALUATE TRUE
               WHEN GRADE IS EQUAL TO "A " 
                 MOVE 4.0 TO TRANS-GRADE
               WHEN GRADE IS EQUAL TO "B+" 
                 MOVE 3.5 TO TRANS-GRADE
               WHEN GRADE IS EQUAL TO "B " 
                 MOVE 3.0 TO TRANS-GRADE
               WHEN GRADE IS EQUAL TO "C+" 
                 MOVE 2.5 TO TRANS-GRADE
               WHEN GRADE IS EQUAL TO "C " 
                 MOVE 2.0 TO TRANS-GRADE
               WHEN GRADE IS EQUAL TO "D+" 
                 MOVE 1.5 TO TRANS-GRADE
               WHEN GRADE IS EQUAL TO "D " 
                 MOVE 1.0 TO TRANS-GRADE
               WHEN GRADE IS EQUAL TO "F " 
                 MOVE 0.0 TO TRANS-GRADE
           END-EVALUATE 

      *    AVG
           COMPUTE SUM-ALL-SCORE = SUM-ALL-SCORE + 
                                      ( SUB-UNIT * TRANS-GRADE )
           ADD SUB-UNIT TO COUNT-ALL-UNIT
              
           
      *    AVG SCI
           IF SINGLE-ID-SCI IS EQUAL TO 3 THEN
              COMPUTE SUM-SCORE-SCI = SUM-SCORE-SCI +
                                      ( SUB-UNIT * TRANS-GRADE )
              ADD SUB-UNIT TO COUNT-SCI-UNIT
           END-IF 
      *    AVG COM-SCI
           IF SINGLE-ID-COMSCI IS EQUAL TO 31 THEN
              COMPUTE SUM-SCORE-COM-SCI  = SUM-SCORE-COM-SCI +
                                      ( SUB-UNIT * TRANS-GRADE )
              ADD SUB-UNIT TO COUNT-COM-SCI-UNIT
           END-IF 
           .     
       001-EXIT.
           EXIT
           . 
       002-AVG.
           COMPUTE AVG         = SUM-ALL-SCORE / COUNT-ALL-UNIT  
           COMPUTE AVG-SCI     = SUM-SCORE-SCI / COUNT-SCI-UNIT  
           COMPUTE AVG-COM-SCI = SUM-SCORE-COM-SCI  / COUNT-COM-SCI-UNIT

           DISPLAY "ALL UNIT            : " COUNT-ALL-UNIT 
           DISPLAY "SCI UNIT            : " COUNT-SCI-UNIT  
           DISPLAY "COMSCI UNIT         : " COUNT-COM-SCI-UNIT  
           
           DISPLAY "-------------------------------------------------"
           DISPLAY "AVG                 : " AVG
           DISPLAY "AVG SCI             : " AVG-SCI 
           DISPLAY "AVG COM-SCI         : " AVG-COM-SCI 
           DISPLAY "-------------------------------------------------"
           
           MOVE AVG TO AVG-GRADE
           MOVE AVG-COM-SCI TO AVG-CS-GRADE 
           MOVE AVG-SCI TO AVG-SCI-GRADE 
           WRITE AVG-GRADE-DETAIL  
           .